Rails.application.routes.draw do

  devise_for :users, path_names: { sign_in: 'login', sign_out: 'logout' }

  root to: 'posts#index'
  resources :categories do
    resources :posts
    resources :comments, only: %i[create destroy], module: :categories
  end
  resources :posts, only: %i[show index] do
    resources :comments, only: %i[create destroy], module: :posts
  end
end
