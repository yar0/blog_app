categories = []
5.times do
  name = FFaker::Food.fruit + ' and ' + FFaker::Music.genre
  categories << Category.create(name: name,
                                description: FFaker::Lorem.paragraph)
end
posts = []
50.times do
  name = FFaker::Music.artist + ' | ' + FFaker::Music.song
  content = FFaker::Lorem.paragraph(rand(3..6))
  posts << { name: name, content: content, category: categories.sample }
end

Post.create(posts)