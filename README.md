# README

##Тестове завдання Blog

### Що реалізовано

* Автентифікація та авторизація користувачів.
* CRUD для категорій.
* CRUD для постів.
* Додавання коментарів до категорій та постів без перезавантаження сторінки.
Видалення коментарів автором або адміном.
 
###Використані бібліотеки:

* **carrierwave** - використувавалась для завантаженнь файлів.
 Чому? - представляє гнучкий та багатий інтерфейс для завантаження файлів.
  Вкрита тестами, підтримується розробниками.

* **bootstrap** - інтеграція фронтенд фреймворку Bootstrap 4.

* **jquery-rails** - інтеграція JQuery в rails 

* **devise** - реалізація автентифікації.
 Чому? - гнучкий, багатий функціонал, популярний серед розробників, розширюваний 
 
* **simple_form** - гнучке рішення для створення форм

* **cancanca** - авторизація користувачів. Чому? - гнучкий, популярний серед розробниів,
  інтегрується із іншими гемами (devise, activeadmin etc.), покритий тестами
   
* **rspec_rails** - інтеграція RSpec, використовувався для реалізації тестів.
 Чому? - можна вважати стандартим інструментом тестування Ruby проектів.

* **rails-controller-testing** - додаткові методи для тестування контроллерів.

* **factory_bot_rails** - фабрики для генерації тестових даних.

* **database_cleaner** - налаштування очистки БД після виконання тестів.

* **shoulda_matchers** - хелпери для тестування (валідацій, асоціацій...).

* **annotate** - додає анотації, до класів моделей, у вигляді структури таблиць.

* **ffaker** - генерація фейкових даних.

### Run
```
> git clone https://yar0@bitbucket.org/yar0/blog_app.git
> cd blog_app
> rake db:create
> rake db:migrate
> rake db:seed
> rais s
```
[Відкрити додаток](http://localhost:3000)

### TODO
реалізувати тести, налаштувани кешування фрагментів, адаптивна верстка.