# == Schema Information
#
# Table name: categories
#
#  id          :bigint(8)        not null, primary key
#  description :text
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_categories_on_name  (name) UNIQUE
#

FactoryBot.define do
  factory :category do
    sequence(:name) { |n| "Super category #{n}." }
    description 'Category description'
  end
end
