# == Schema Information
#
# Table name: posts
#
#  id          :bigint(8)        not null, primary key
#  content     :text
#  file        :string
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :bigint(8)
#
# Indexes
#
#  index_posts_on_category_id  (category_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#

FactoryBot.define do
  factory :post do
    category
    sequence(:name) { |n| "Post number #{n}." }
    content 'Post content..'
    file 'file_url'
  end
end
