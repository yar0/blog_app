# == Schema Information
#
# Table name: users
#
#  id                 :bigint(8)        not null, primary key
#  email              :string           default(""), not null
#  encrypted_password :string           default(""), not null
#  first_name         :string
#  last_name          :string
#  role               :string           default("regular")
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_users_on_email  (email) UNIQUE
#

FactoryBot.define do
  factory :user do
    password 'password'
    sequence(:email) { |n| "test#{n}.user@mail.com" }
    first_name 'Linus'
    last_name 'Torvalds'
  end
end
