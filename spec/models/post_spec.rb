require 'rails_helper'

RSpec.describe Post, type: :model do

  context 'validation' do
    let(:post) { Post.new(category: create(:category)) }
    it { should validate_presence_of(:name) }
    it { should validate_length_of(:name).is_at_most(100) }
    it 'is not valid with when name consist of less than 2 words' do
      post.name = 'abc'

      expect(post).to_not be_valid
    end
    # TODO: refactor, try shared_examples
    it 'is not valid with when name consist of 2 words and word length < 2' do
      post.name = 'abc d'

      expect(post).to_not be_valid
    end
    it 'is valid when has more than 2 words' do
      post.name = 'Post about nothing.'

      expect(post).to be_valid
    end
    it 'invalid when file size is more than 2 megabytes' do
      allow(post.file).to receive(:size).and_return(3.megabytes)
      post.valid?
      expect(post.errors[:file]).to_not be_empty
    end
  end
  context 'associations' do
    it { should belong_to(:category) }
    it { should have_many(:comments) }
  end
end
