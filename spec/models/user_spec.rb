require 'rails_helper'

RSpec.describe User, type: :model do
  context 'validation' do
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }
    it do
      should validate_length_of(:first_name)
                 .is_at_least(2)
                 .is_at_most(50)
    end
    it do
      should validate_length_of(:last_name)
                 .is_at_least(2)
                 .is_at_most(50)
    end
  end
  context 'instance methods' do
    let(:user) { create(:user) }
    it '#name returns user first_name' do
      expect(user.name).to eq(user.first_name)
    end
    it '#full_name' do
      expect(user.full_name.downcase).to eq("#{user.first_name} #{user.last_name}".downcase)
    end
    it '#role?' do
      user.role = 'admin'
      expect(user.role?(:admin)).to be_truthy
    end
  end
  it 'has formatted first and last names' do
    user = create(:user, first_name: 'donald', last_name: 'duck')
    expect(user.first_name).to eq('Donald')
    expect(user.last_name).to eq('Duck')
  end
end
