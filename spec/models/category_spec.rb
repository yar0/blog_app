require 'rails_helper'

RSpec.describe Category, type: :model do
  context 'fields validations' do
    let(:category) { Category.new }
    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name) }
    it { should have_many(:posts) }
    it { should have_many(:comments) }
    it { should validate_length_of(:name).is_at_most(50) }
    it 'is not valid with when name consist of less than 2 words' do
      category.name = 'abc'

      expect(category).to_not be_valid
    end
    it 'is not valid with when name consist of 2 words and word length < 2' do
      category.name = 'abc d'

      expect(category).to_not be_valid
    end
    it 'is valid when has more than 2 words, starts with capital letter and contain dot' do
      category.name = 'Word word word.'

      expect(category).to be_valid
    end
  end
end
