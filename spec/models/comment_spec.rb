require 'rails_helper'

RSpec.describe Comment, type: :model do
  context 'validations' do
    it { should validate_presence_of(:content) }
    it { should validate_length_of(:content).is_at_most(400) }
  end
  context 'associations' do
    it { should belong_to(:commentable) }
    it { should belong_to(:user) }
  end
  it '#author' do
    comment = create(:comment)
    expect(comment.author).to eq(comment.user.full_name)
  end
end
