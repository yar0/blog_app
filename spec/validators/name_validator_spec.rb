require 'rails_helper'
# TODO: skip database cleaner, by passing metadata :skip_clean!!!
describe NameValidator do
  let(:validator) { NameValidator.new }
  let(:errors_messages) { Hash[name: []] }

  describe '#validate' do
    it 'add error message when name consist of 1 word' do
      record = double('record', errors: errors_messages, name: 'a')
      validator.validate(record)
      msg = 'should contain minimum 2 words'

      expect(errors_messages[:name]).to include(msg)
    end
    it 'add error message when name consists of 2 words with length < 2' do
      record = double('record', errors: errors_messages, name: 'a da')
      validator.validate(record)
      msg = 'should contain minimum 2 words, with minimum 2 letters'

      expect(errors_messages[:name]).to include(msg)
    end
  end
end