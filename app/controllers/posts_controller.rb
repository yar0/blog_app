class PostsController < ApplicationController
  # TODO: add styles to view
  # TODO: implement pagination
  authorize_resource

  def index
    @posts = fetch_posts
  end

  def show
    @post = Post.find(params[:id])
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    @post.category_id = params[:category_id]
    if @post.save
      redirect_to @post, notice: 'Post successfully created!'
    else
      pp @post.errors.messages
      render :new
    end
  end

  def edit
    @post = Post.find(params[:id])
  end

  def update
    @post = Post.find(params[:id])
    if @post.update_attributes(post_params)
      redirect_to @post, notice: 'Post successfully updated!'
    else
      render :edit
    end
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    redirect_to category_posts_path(@post.category_id), notice: 'Post successfully deleted!'
  end

  private

  def fetch_posts
    if params[:category_id]
      Post.where(category_id: params[:category_id])
    else
      Post.all
    end
  end

  def post_params
    params.require(:post).permit(:name, :content, :file, :category_id)
  end
end
