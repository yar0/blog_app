class CategoriesController < ApplicationController
  authorize_resource

  def show
    @category = Category.find(params[:id])
  end

  def index
    @categories = Category.all
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)

    if @category.save
      redirect_to category_path(@category), notice: 'New category created!'
    else
      render :new
    end
  end

  def edit
    @category = Category.find(params[:id])
  end

  def update
    @category = Category.find(params[:id])
    if @category.update_attributes(category_params)
      redirect_to @category, notice: 'Successfully updated!'
    else
      render :edit
    end
  end

  def destroy
    Category.destroy(params[:id])
    redirect_to categories_path, notice: 'Successfully deleted!'
  end

  private

  def category_params
    params.require(:category).permit(:name, :description)
  end
end
