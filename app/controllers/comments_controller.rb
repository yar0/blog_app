class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    @comment = @commentable.comments.new comment_params
    @comment.user = current_user
    authorize! :create, @comment
    if @comment.save
      comment_html = render_to_string(template: 'comments/_comment', layout: false,
                                      locals: { comment: @comment })
      render json: { comment: comment_html }
    else
      error_msg = "Comment content #{@comment.errors.messages[:content].first}."
      render json: { error: error_msg }, status: :unprocessable_entity
    end
  end

  def destroy
    @comment = @commentable.comments.find(params[:id])
    authorize! :delete, @comment
    @comment.destroy
    render json: { comment_id: @comment.id }
  end

  private

  def comment_params
    params.require(:comment).permit(:content)
  end
end
