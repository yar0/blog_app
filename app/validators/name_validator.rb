class NameValidator < ActiveModel::Validator

  def validate(record)
    min_words_count = 2
    min_word_length = 2
    return if record.name.nil?
    words = record.name.split(' ')
    if words.count < min_words_count
      record.errors[:name] << "should contain minimum #{min_words_count} words"
    elsif words.count == min_words_count
      msg = "should contain minimum #{min_words_count} words," \
          " with minimum #{min_word_length} letters"
      record.errors[:name] << msg if words.any? { |w| w.length < min_word_length }
    end
    # TODO: write specs
    if record.name.starts_with?(/[a-z]/)
      record.errors[:name] << 'should starts with a capital letter.'
    end
    record.errors[:name] << "should include '.'" unless record.name.include? '.'
  end
end