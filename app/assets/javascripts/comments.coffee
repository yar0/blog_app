# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

handleCommentRemove = (event) ->
  data = event.detail[0]
  $('#comment_' + data.comment_id).remove()

handleCommentRemovingError = (event) ->
  console.log("Can't remove comment.")

handleNewComment = (event) ->
  data = event.detail[0]
  $('#comments').append(data.comment)
  $('#comment_content').val('')
  $('#comment_content').removeClass('is-invalid')

handleNewCommentError = (event) ->
  data = event.detail[0]
  console.log(data.error)
  $("#comment_content").addClass('is-invalid')
  $("#new_comment .invalid-feedback").text(data.error)

$(document).on "turbolinks:load", ->
  $("#new_comment").on("ajax:success",
    handleNewComment
  ).on("ajax:error", handleNewCommentError)


  $('#comments')
    .on("ajax:success", 'a.delete_comment', handleCommentRemove)
    .on("ajax:error", 'a.delete_comment', handleCommentRemovingError)