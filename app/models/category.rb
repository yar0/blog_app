# == Schema Information
#
# Table name: categories
#
#  id          :bigint(8)        not null, primary key
#  description :text
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_categories_on_name  (name) UNIQUE
#

class Category < ApplicationRecord
  include Concerns::Commentable

  validates :name, presence: true, uniqueness: true, length: { maximum: 50 }
  validates_with NameValidator

  has_many :posts, dependent: :destroy
end
