class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new(role: 'guest')

    if user.role?(:admin)
      can :manage, :all
    elsif user.role?(:regular)
      can :delete, Comment, user_id: user.id
      can :create, Comment
    end
    can :read, Post
    can :read, Category
  end
end
