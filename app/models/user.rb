# == Schema Information
#
# Table name: users
#
#  id                 :bigint(8)        not null, primary key
#  email              :string           default(""), not null
#  encrypted_password :string           default(""), not null
#  first_name         :string
#  last_name          :string
#  role               :string           default("regular")
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_users_on_email  (email) UNIQUE
#

class User < ApplicationRecord
  devise :database_authenticatable, :registerable, :validatable

  before_create :format_names
  before_update :format_names
  validates :first_name, presence: true, length: { in: 2..50 }
  validates :last_name, presence: true, length: { in: 2..50 }

  has_many :comments, dependent: :destroy
  def name
    first_name
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def role?(tested_role)
    role == tested_role.to_s
  end

  private

  def format_names
    first_name[0] = first_name[0].upcase if first_name
    last_name[0] = last_name[0].upcase if last_name
  end
end
