# == Schema Information
#
# Table name: posts
#
#  id          :bigint(8)        not null, primary key
#  content     :text
#  file        :string
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :bigint(8)
#
# Indexes
#
#  index_posts_on_category_id  (category_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#

class Post < ApplicationRecord
  include Concerns::Commentable

  validates :name, presence: true, length: { maximum: 100 }
  validates_with NameValidator
  validate :max_file_size

  belongs_to :category

  mount_uploader :file, FileUploader

  private

  def max_file_size
    errors[:file] << 'file size can not be more than 2 megabytes.' if file.size > 2.megabytes
  end

end
